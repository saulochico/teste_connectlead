<?php

/**
 * Created by PhpStorm.
 * User: gilso
 * Date: 07/01/2016
 * Time: 19:50
 */
class MY_Model extends CI_Model
{
    public $regras = []; //array pra ser sobrescrito nos models

    public function __contruct()
    {
        parent::__construct();
    }
    
    public function validar() // metodo generico pra todos os models.
    {
        $this->form_validation->set_rules($this->regras);
    }

    public function salvar($dados, $tabela, $campo, $id = "")
    {
        if(empty($id)){
            $this->db->insert($tabela, $dados);
            return $this->db->insert_id();
        }
        else{
            $this->db->where($campo, $id);
            $this->db->update($tabela, $dados);
            return $id;
        }
    }
}