<?php

/**
 * Created by PhpStorm.
 * User: gilso
 * Date: 07/01/2016
 * Time: 19:49
 */

require APPPATH."third_party/MX/Controller.php";

class MY_Controller extends MX_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->output->enable_profiler(false);
    }

    public function set_data(&$obj, $dados)
    {
        foreach($dados as $k => $v){
            $obj->$k = $v;
        }
    }
}
