<div class="container marketing">
    <!-- START THE FEATURETTES -->
    <?php foreach ($noticias as $i => $noticia): ?>
        <hr class="featurette-divider">
        <div class="row featurette">
            <div class="col-md-12" style="display: flex; flex-direction: row; justify-content: center; align-items: center">
                <div class="col-md-7<?php echo $i % 2 == 0 ? " col-md-push-5" : ""; ?>">
                    <h1 class="heading"><?php echo $noticia->titulo ?><br>
                        <small class="text-muted">Fonte: <?php echo $noticia->fonte ?></small>
                    </h1>
                    <p class="lead"><?php echo substr($noticia->noticia, 0,50)."..."?></p>
                    <a class="btn btn-info" href="noticia/<?php echo $noticia->idnoticia?>/<?php echo str_replace(" ", "-", $noticia->titulo)?>">Saiba Mais</a>
                </div>
                <div class="col-md-5<?php echo $i % 2 == 0 ? " col-md-pull-7" : ""; ?>">
                    <img class="featurette-image img-responsive center-block thumbnail"
                         src="<?php echo base_url("imagens/" . $noticia->url_imagem) ?>"
                         alt="<?php echo $noticia->url_imagem?>">
                </div>
            </div>
        </div>
    <?php endforeach; ?>
    <!-- /END THE FEATURETTES -->
    <div class="row">
        <div class="col-md-12">
            <?php echo $this->pagination->create_links(); ?>
        </div>
    </div>
    <!-- FOOTER -->
    <footer>
        <p class="pull-right"><a href="#">Back to top</a></p>
        <p>&copy; 2017 Saulo Rocha &middot;</p>
    </footer>

</div>