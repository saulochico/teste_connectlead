<div class="container marketing">

    <!-- START THE FEATURETTES -->
    <?php foreach ($noticias as $i => $noticia): ?>
        <hr class="featurette-divider">
        <div class="row featurette">
            <div class="col-md-7<?php echo $i % 2 == 0 ? " col-md-push-5" : ""; ?>">
                <h2 class="heading"><?php echo $noticia->titulo ?><br>
                    <small class="text-muted">Fonte: <?php echo $noticia->fonte ?></small>
                </h2>
                <p class="lead"><?php echo substr($noticia->noticia, 0,20); ?></p>
                <a class="btn btn-info" href="<?php echo base_url()?>/noticia/<?php echo $noticia->idnoticia?>/<?php echo str_replace(" ", "-", $noticia->titulo)?>">Saiba Mais</a>
            </div>
            <div class="col-md-5<?php echo $i % 2 == 0 ? " col-md-pull-7" : ""; ?>">
                <img class="featurette-image img-responsive center-block thumbnail"
                     src="<?php echo base_url("imagens/" . $noticia->url_imagem) ?>" alt="<?php echo $noticia->url_imagem?>">
            </div>
        </div>
    <?php endforeach; ?>
    <!-- /END THE FEATURETTES -->