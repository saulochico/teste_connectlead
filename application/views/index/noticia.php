<div class="container" style="padding-top: 100px;">
    <div class="row">
        <div class="col-md-12">
            <img class="img-responsive thumbnail" src="<?php echo base_url("imagens/" . $noticia->url_imagem) ?>">
        </div>
    </div>
    <div class="row ">
        <div class="col-lg-12">
            <h1><?php echo $noticia->titulo ?></h1><br>
            <small><strong><?php echo $noticia->categoria ?></strong></small>
            <p style="font-size: 1.5em; text-align: justify;"><?php echo $noticia->noticia ?></p>
            <small><?php echo "Notícia de " . $noticia->data . ", escrita por " . $noticia->fonte ?></small>
        </div>
    </div>
    <a class="btn btn-info" href="<?php echo base_url();?>"><i class="glyphicon glyphicon-arrow-left"></i> Voltar</a>
</div>