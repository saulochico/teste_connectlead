<!-- Carousel
================================================== -->
<div id="myCarousel" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
    <ol class="carousel-indicators">
        <?php foreach ($carousels as $i => $carousel):?>
            <li data-target="#myCarousel" data-slide-to="<?php echo $i; ?>" <?php echo $i == 0 ? 'class="active"' : ""; ?></li>
        <?php endforeach;?>
    </ol>
    <div class="carousel-inner" role="listbox">
        <?php foreach ($carousels as $i => $carousel):?>
        <div class="item <?php echo $i == 0 ? "active" : ""; ?>">
            <img class="first-slide" src="<?php echo base_url("imagens/".$carousel->url_imagem)?>" alt="First slide">
            <div class="container">
                <div class="carousel-caption">
                    <h1><?php echo $carousel->titulo?></h1>
                    <p>Fonte: <?php echo $carousel->fonte ?>
                </div>
            </div>
        </div>
        <?php endforeach;?>
    </div>
    <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a>
</div><!-- /.carousel -->