<?php
//require_once 'system/libraries/Form_v';
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of MY_Form_Validation
 *
 * @author gilso
 */
class MY_Form_Validation extends CI_Form_validation {

    public function __construct($rules = array()) {
        parent::__construct($rules = array());
    }

    public function unique($value, $params) {
        $CI = & get_instance(); //pega a instacia do CI
        $CI->load->database(); //carrega o banco de dados

        $CI->form_validation->set_message('unique', "Sorry, that %s is already being used."); // seta a mensagem de validação

        //explode os parametros por ponto.
        $explode = explode(".", $params);
        $table = $explode[0];
        $field = $explode[1];
        $current_id = $explode[2];
        $pk = $explode[3];
        
        //se tiver o terceiro parametro, current_id, tá editando.
        //aqui que lascou, pois não consigo separar isso no array $regras de validação.
        if (isset($current_id)) {
            $query = $CI->db->select()->from($table)->where($field, $value)->limit(1)->get();
            
            if ($query->row() && $query->row()->$pk != $current_id) {
                return FALSE;
            }
        } else {
            //caso não tiver $current_id, tá validando uma nova inserção
            $query = $CI->db->select($field)->from($table)
                            ->where($field, $value)->limit(1)->get();

            if ($query->row()) {
                return false;
            }
        }
        return true;
    }

}
