<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Api
{
    public $ci;
    private $base_url;
    private $api;

    public function __construct()
    {
        $this->ci = &get_instance();
        $this->base_url = config_item('base_api_url');

        $this->api = new RestClient([
            'base_url' => $this->base_url
        ]);
    }

    public function get($url, Array $params = [], Array $headers = [])
    {
        $result = $this->api->get($url, $params, $headers);
        if ($result->info->http_code == 200) {
            return json_decode($result->response);
        } else {
            throw new Exception("Erro ao buscar informações em API");
            http_response_code($result->info->http_code);
        }
    }

    public function post($url, Array $params, Array $headers = [])
    {
        $result = $this->api->post($url, $params, $headers);

        if ($result->info->http_code == 200) {
            return json_decode($result->response);
        } else {
            throw new Exception("Erro ao buscar informações em API");
            http_response_code($result->info->http_code);
        }
    }
}