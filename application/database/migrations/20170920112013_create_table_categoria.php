<?php

class Migration_create_table_categoria extends CI_Migration {

    public function up() {
        $this->dbforge->add_field(array(
            'idcategoria' => array(
                'type' => 'INT',
                'constraint' => 11,
                'auto_increment' => TRUE
            ),
            'categoria' => array(
                'type' => 'VARCHAR',
                'constraint' => 200,
            ),
        ));
        $this->dbforge->add_key('idcategoria', TRUE);
        $this->dbforge->create_table('categoria');
    }

    public function down() {
        $this->dbforge->drop_table('categoria');
    }

}