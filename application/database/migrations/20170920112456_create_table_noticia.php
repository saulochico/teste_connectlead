<?php

class Migration_create_table_noticia extends CI_Migration {

    public function up() {
        $this->dbforge->add_field(array(
            'idnoticia' => array(
                'type' => 'INT',
                'constraint' => 11,
                'auto_increment' => TRUE
            ),
            'titulo' => array(
                'type' => 'VARCHAR',
                'constraint' => 100,
            ),
            'noticia' => array(
                'type' => 'TEXT',
            ),
            'fonte' => array(
                'type' => 'VARCHAR',
                'constraint' => 100,
            ),
            'destaque' => array(
                'type' => 'INT',
                'constraint' => 1,
            ),
            'url_imagem' => array(
                'type' => 'VARCHAR',
                'constraint' => 200,
            ),
            'data' => array(
                'type' => 'date',
            ),
            'categoria_id' => array(
                'type' => 'INT',
                'constraint' => 11,
            ),
        ));
        $this->dbforge->add_key('idnoticia', TRUE);
        $this->dbforge->create_table('noticia');
        $this->dbforge->add_column('noticia', [
            'CONSTRAINT fk_categoria FOREIGN KEY(categoria_id) REFERENCES categoria(idcategoria)'
        ]);
    }

    public function down() {
        $this->dbforge->drop_table('noticia');
    }

}