<?php

/**
 * Created by PhpStorm.
 * User: saulo
 * Date: 20/09/17
 * Time: 14:36
 */
class Noticia extends MY_Model
{
    public function listarTodos($offset = 0)
    {
        return $this->db->select("*")
            ->from("noticia")
            ->order_by("data desc")
            ->limit(5, $offset)
            ->get()->result();
    }

    public function listarDestaque()
    {
        $this->db->where("destaque", 1);
        return $this->db->get("noticia")->result();
    }

    public function contarNoticias()
    {
        return $this->db->select('*')
            ->from('noticia')
            ->count_all_results();
    }

    public function listarPorCategoria($idcategoria)
    {
        $this->db->where("categoria_id", $idcategoria);
        return $this->db->get("noticia")->result();
    }

    public function buscarPorId($idnoticia)
    {
        return $this->db->select('*')
                        ->from("noticia n")
                        ->join("categoria c", "c.idcategoria = n.categoria_id")
                        ->where("idnoticia", $idnoticia)
                        ->get()->row();
    }
}