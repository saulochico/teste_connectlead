<?php

/**
 * Created by PhpStorm.
 * User: saulo
 * Date: 20/09/17
 * Time: 18:29
 */
class Categoria extends MY_Model
{
    public function listarTodos()
    {
        return $this->db->select("*")
            ->from("categoria")
            ->order_by("categoria asc")
            ->get()->result();
    }
}