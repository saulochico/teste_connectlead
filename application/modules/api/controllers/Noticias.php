<?php

class Noticias extends MY_Controller
{
    public function __construct()
    {
        $this->load->model("Noticia");

        parent::__construct();
    }

    public function listarNoticias($offset = 0)
    {
        $noticias = $this->Noticia->listarTodos($offset);

        $this->output
            ->set_status_header(200)
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($noticias))
            ->_display();
        exit;
    }

    public function listarDestaques()
    {
        $destaque = $this->Noticia->listarDestaque();

        $this->output
            ->set_status_header(200)
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($destaque))
            ->_display();
        exit;
    }

    public function contarTodos()
    {
        $result['total_rows'] = $this->Noticia->contarNoticias();

        $this->output
            ->set_status_header(200)
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($result))
            ->_display();
        exit;
    }

    public function listarPorCategoria($idcategoria)
    {
        $noticias = $this->Noticia->listarPorCategoria($idcategoria);

        $this->output
            ->set_status_header(200)
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($noticias))
            ->_display();
        exit;
    }

    public function buscarNoticia($idnoticia)
    {
        $noticia = $this->Noticia->buscarPorId($idnoticia);

        $this->output
            ->set_status_header(200)
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($noticia))
            ->_display();
        exit;
    }
}
