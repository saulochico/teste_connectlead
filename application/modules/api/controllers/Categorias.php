<?php

/**
 * Created by PhpStorm.
 * User: saulo
 * Date: 20/09/17
 * Time: 18:29
 */
class Categorias extends MY_Controller
{
    public function __construct()
    {
        $this->load->model("Categoria");

        parent::__construct();
    }

    public function listarCategorias()
    {
        $categorias = $this->Categoria->listarTodos();

        $this->output
            ->set_status_header(200)
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($categorias))
            ->_display();
        exit;
    }
}