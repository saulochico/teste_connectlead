<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Index extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index($offset = 0)
    {
        $retorno['noticias'] = $this->api->get("noticias/listarNoticias/" . $offset);
        $retorno['carousels'] = $this->api->get("noticias/listarDestaques");
        $retorno['categorias'] = $this->api->get("categorias/listarCategorias");

        $pag = $this->api->get("noticias/contarTodos");
        $config['total_rows'] = $pag->total_rows;
        $config['base_url'] = base_url('/page');

        $this->pagination->initialize($config);

        $this->layout->view('index/noticias', $retorno);
    }

    public function listarPorCategoria($idcategoria)
    {
        $retorno['noticias'] = $this->api->get("noticias/listarPorCategoria/" . $idcategoria);
        $retorno['categorias'] = $this->api->get("categorias/listarCategorias");
        $this->layout->view('index/listarporcategoria', $retorno);
    }

    public function Noticia($idnoticia)
    {
        $retorno['noticia'] = $this->api->get("noticias/buscarNoticia/" . $idnoticia);
        $retorno['categorias'] = $this->api->get("categorias/listarCategorias");

        $this->layout->view('index/noticia', $retorno);
    }
}
